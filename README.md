# godot-scripts
Collection of scripts for godot engine.

## Installation
Scripts can be installed globally with `./install.sh`.

They all have `gd-` prefix so autocompletion should help out with their names
if you forget them.

## LICENSE
All of them are licensed under MIT and should have the license itself
on top of the file with proper acknowledgements of other works which they were based on.
