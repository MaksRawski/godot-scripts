#! /usr/bin/python3
"""
Generates an AnimatedTexture resource file from a list of
numbered images.

This is a more user-friendly and featureful version of Fernando Cosentino's:
https://github.com/fbcosentino/godot-AnimatedTexture-from-blender

MIT License

Copyright (c) 2022 Maksymilian Rawski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from typing import Tuple

import os
import sys
import argparse
import re

parser = argparse.ArgumentParser(description="Generates an AnimatedTexture resource file from a list of numbered images.")

parser.add_argument("image_format", type=str,
                    help="printf style format string to use for finding images, e.g. assets/animation/image_%%04d.png")

parser.add_argument("-s", dest="start", type=int, default=0, help="number of start frame (default: 0)")
parser.add_argument("-e", dest="end",   type=int, default=-1, help="number of end frame (default: -1 (all matching))")
parser.add_argument("-o", dest="output",type=str, default="output.tres", help="filename for output texture (default: output.tres)")
parser.add_argument("-n",dest="no_check", action="store_const", const=True, default=False, help="Don't check if all files are present (default: false)")
parser.add_argument("-r", dest="reverse", action="store_const", const=True, default=False, help="Go through files in reverse order (default: false)")


class CFormatter:
    """Class for parsing very limited printf style formatting"""
    def __init__(self, format_str: str):
        # padding is given just a single digit as having more than bilion frames
        # probably won't even be possible
        m = re.match(r"^(?P<path>.+/)*(?P<filename_prefix>.*)?"
                    r"%(?P<delimiter>\w)(?P<padding>\d)d(?P<filename_suffix>.*)?"
                    r"(?P<extension>\.\w+)?$", format_str)
        if not m:
            print("Format string must contain '%(delimiter)(padding)d'", file=sys.stderr)
            exit(1)

        self.format_str = ("./" if not m.group("path") else "") + format_str
        if m.group("path")[0] == "/":
            print("DO NOT USE ABSOLUTE PATHS!", file=sys.stderr)
            exit(1)

        self.parsed_fmt: dict[str, str] = m.groupdict()
        # self.parsed_fmt["path"] = "./" if not m.group("path") else m.group("path")

    def __getitem__(self, item: str) -> str:
        return self.parsed_fmt[item] or ""

    def _pad_number(self, num: int) -> str:
        pad_char: str = self.parsed_fmt["delimiter"]
        pad_len: int = int(self.parsed_fmt["padding"])
        num_len = len(str(num))

        if num_len > pad_len:
            print("Number doesn't fit in the formating!",file=sys.stderr)
            exit(1)

        return (pad_len - num_len)*pad_char + str(num)

    def format(self, num: int) -> str:
        padded_num = self._pad_number(num)

        return self["path"] + self["filename_prefix"] + padded_num + \
            self["filename_suffix"] + self["extension"]


def last_file_found(formatter: CFormatter, start: int, end: int) -> Tuple[bool, int]:
    get_filename = lambda p: p[len(formatter["path"]):]
    files = os.listdir(formatter["path"] or "./")
    file_pattern = re.sub(r"%.\dd", r"(\\d+)", get_filename(formatter.format_str))
    print(file_pattern)

    # all present matching files
    matching_files = list(filter(lambda f: re.match(f"^{file_pattern}$", f), files))
    print(matching_files)

    if len(matching_files) == 0:
        print("No matching files found!")
        exit(1)

    i = start
    while i <= end or end == -1:
        # if file is missing
        current_file = get_filename(formatter.format(i))
        if not current_file in matching_files:
            return (False, i)
        i += 1
    # all files were found, return last one
    return (True, i - 1)


def generate(formatter: CFormatter, skip: int, frames: int, reverse: bool) -> str:
    """Generates the string content of the file"""
    result = "[gd_resource type=\"AnimatedTexture\" load_steps="+str(frames+1)+" format=2]\n\n"
    # Temporary variable holding ext_resource section
    ext_res = ""
    # Temporary variable holding frames section
    frames_res = "[resource]\nflags = 7\nframes = "+str(frames)+"\nfps = 24.0\n"
    
    for i in range(frames):
        # File number
        num = i+1
        # Add file to ext_resource section
        ext_res += "[ext_resource path=\"res://"+formatter.format( skip+frames-num if reverse else skip+num )+"\" type=\"Texture\" id="+str(num)+"]\n"
        # Add item to frames section
        frames_res += "frame_"+str(i)+"/texture = ExtResource( "+str(num)+" )\n"
        if i > 0:
            frames_res += "frame_"+str(i)+"/delay_sec = 0.0\n"
            
    result += ext_res+"\n"+frames_res
    return result


if __name__ == "__main__":
    args = parser.parse_args()

    if not "project.godot" in os.listdir():
        print("""godot.project not found in current directory!
Make sure you run this script from within the root of your project,
as all the paths need to be relative to it""", file=sys.stderr)
        exit(1)

    if args.start > args.end and not args.reverse and args.end != -1:
        print("START can't have bigger value than END! Consider using -r flag.", file=sys.stderr)
        exit(1)

    if args.reverse and args.end == -1:
        args.end = 0

    start, end = (args.end, args.start) if args.reverse and args.start > args.end else (args.start, args.end)

    formatter = CFormatter(args.image_format)
    file_pattern = re.sub(r"%.\dd", r"(\\d+)", args.image_format)
    last_file = last_file_found(formatter, start, end)

    if not last_file[0] and not args.no_check :
        print(f"File {formatter.format(last_file[1])} doesn't exist!", file=sys.stderr)
        exit(1)

    end = last_file[1]
    result = generate(formatter, start, end - start + 1, args.reverse)

    with open(args.output, "w") as f:
        f.write(result)

    print(f"Succesfully generated: {args.output}")
